# Lymm's Binoculars
# All credit goes to Lymm for this script. I only uploaded it.

## What is it?
This is a python script to find the mysterious [Eye Messages](https://noita.wiki.gg/wiki/Eye_Messages) for any seed in the game [Noita](https://noitagame.com/)

## Usage Notes

Chillie created [this handy web tool](https://chillie-ilya.github.io/lymms-binoculars-web/) for ease of use and convenience.

On Windows I highly recommend using [Chocolatey](https://chocolatey.org/install). With this package manager installed and the script downloaded you can just simply use the following commands:
  * `choco install python3`
  * `pip install numpy`
  * `python "X:\Path\To\lymmsbinoculars.py"`

The script will ask you to provide a seed to search, and then it will provide that seed's eye message coordinates.